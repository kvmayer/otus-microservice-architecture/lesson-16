## Только HTTP взаимодействие
[OpenAPI](RESTful.openapi.yaml)

![RESTful](RESTful.png)

## Событийное взаимодействие с использование брокера сообщений для нотификаций (уведомлений)
[OpenAPI](EventNotification.openapi.yaml)

[AsyncAPI](EventNotification.asyncapi.yaml)

![EventNotification](EventNotification.png)

## Event Collaboration cтиль взаимодействия с использованием брокера сообщений
[AsyncAPI](EventCollaboration.asyncapi.yaml)

![EventCollaboration](EventCollaboration.png)

## Свой вариант. HTTP + RPC over message bus
OrderService выполняет роль оркестратора. Принимает запрос по http, остальные операции выполняются асинхронно через брокер сообщений.

[OpenAPI](RESTful+RPC.openapi.yaml)

[AsyncAPI](RESTful+RPC.asyncapi.yaml)

![RESTful+RPC](RESTful+RPC.png)
